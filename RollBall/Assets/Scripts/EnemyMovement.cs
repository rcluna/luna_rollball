﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMovement : MonoBehaviour
{
    public GameObject Player;
    public float speed;
    public int MinDist = 5;
    


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.LookAt (Player.transform);

        if (Vector3.Distance(transform.position, Player.transform.position)>=MinDist);
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
    }
}