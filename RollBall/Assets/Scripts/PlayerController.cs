﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    private Rigidbody rb;
    private int count;
    public int time;
    public GameObject Player;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");
        

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

        rb.AddForce (movement * speed);
    }
    
    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag ( "Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }
    
           
    }
     [System.Obsolete]
    void OnCollisionEnter(Collision other)
    {   
        if (other.gameObject.CompareTag("Enemy"))
            { 
            Application.LoadLevel(Application.loadedLevel);
            }
        if (other.gameObject.CompareTag("Teleporter"))

            Player.transform.position = new Vector3(-8,3,8);
        if (other.gameObject.CompareTag("Spike"))
            { 
            Application.LoadLevel(Application.loadedLevel);
            }      
    }


    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= 12)
        {
            winText.text = "You Win!";

        }
    }
}
